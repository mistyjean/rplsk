#include <iostream>
#include <string>
#include <stdlib.h>


using namespace std;

string get_choice()
  {
      string choice;
      cout << "Rock, Paper, Scissors, Lizard, spocK? " << endl;
      cin >> choice;
      return choice;
  }
string make_guess()
  {
    int dice; string choice;
    srand ( time(NULL) );
    dice = rand () % 5 + 1;
    switch (dice)
      {
        case 1:
          choice= "Rock";
          break;
        case 2:
          choice= "spocK";
          break;
        case 3:
          choice= "Paper";
          break;
        case 4:
          choice= "Lizard";
          break;
        case 5:
          choice= "Scissors";
          break;
      }
    return choice;
  }
class Game{
    private: string pick;
    private: string play;
    public: Game(string choice=get_choice(),
                 string guess=make_guess())
      {
        pick=choice;
        play=guess;

      }
    string resolve()
      {
        cout << "You chose : " << this->pick << endl;
        cout << "I choose : " << this->play << endl;
        return "";
      }
  };
int main(int argc, char *argv[])
  {
    Game Game;
    Game.resolve();
    return 0;
  }
